from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
	"https://files.alerta.rcnradio.com/alerta_bogota/public/styles/amp_metadata_content_image_min_696px_wide/public/2019-08/meme_de_lna_mujer_gritando_y_el_gato_en_la_mesa.jpg?itok=jd1qUF5v",
	"https://fotos.perfil.com/2020/04/14/trim/728/500/barbijos-y-mascarillas-memes-y-fotos-940152.jpg",
	"https://pxb.cdn.ciudadanodiario.com.ar/ciudadano/032020/1585661155584/meme-53.jpg"
    ]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
